#ifndef MP_H
#define MP_H

#include "RHF.h"
#include "Integral.h"
#include "Geometry.h"

class MP{
public:
	int size;
	double Emp2;

	int getIntegrals(Integral&);
	int getOrbitals(RHF&);
	int transformIntegrals(RHF&);
	int computeMP2Energy();
	int mp2(Geometry&, Integral&, RHF&);

	MP(){};
	~MP(){};
private:
	int* indexcache;
	int Ne;
	double* VeeMO;
	double* VeeAO;
	double* orbitals;
	double* energies;
};

#endif

