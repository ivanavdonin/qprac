#ifndef TIMER_H
#define TIMER_H

#include "time.h"
#include "stdio.h"

class Timer{
public:
    float stop();
    Timer();
    ~Timer(){};
private:
    clock_t start;
};

#endif
