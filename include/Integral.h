#ifndef INTEGRAL_H
#define INTEGRAL_H

#include "Basis.h"
#include "Geometry.h"

class Integral{
public:
    double Vnn;
    double* S;
    double* T;
    double* Ven;
    double* Vee;
    int numberVee; // the number of non-zero integrals which is less than the number of required integrals 
    
    int InitAndCalcAll(const Basis&, const Geometry&);

    int BasisSize;
    
    int printMatrix(double*);

    Integral(){};
    ~Integral(){};
private:
    long unsigned int* indexcache;

    double calcVnn(const Geometry&);
    double overlapGTO(const int&, const int&, const int&, const int&, 
    const int&, const int&, const int&, const Basis&);
    double calcS(const int &, const int &, const Basis &);
    double calcT(const int &, const int &, const Basis &);
    double calcVen(const int &, const int &, const int &, const Basis &,
                   const Geometry&);

    double calcVee(const int&, const int&, 
    	           const int&, const int&, const Basis&);

	int calcEijt(double* , const int& , const int& , const double& , const double& , const double& , const double& );
	int calcRntuv(double* , const int& , const int& , const int& , const double& , const double& , const double& , const double& );
    double Vijkl(const int& , const int& , const int& , const double& , const double& , const double& , const double& ,
                     const int& , const int& , const int& , const double& , const double& , const double& , const double& ,
                     const int& , const int& , const int& , const double& , const double& , const double& , const double& ,
                     const int& , const int& , const int& , const double& , const double& , const double& , const double& );

    double calcMDVee(const int& i, const int& j,
                         const int& k, const int& l, const Basis& basis);

};

#endif
