#ifndef RHF_H
#define RHF_H

#include "Geometry.h"
#include "Integral.h"
#include "OurMath.h"

class RHF{
public:
    double* S;
    int size;
    double deltaE;
    double deltaD;
    int maxIter;

    double E;

    double* MOenergies;
    double* MOvectors;

    int buildHcore(const Integral&);
    int printHcore();
    int printMatrix(double*);
    int printMOenergies();
    double* getMatrix(double*);
    double* getVee(Integral&);

    int buildOrthogonalizationMatrix(Integral&);
    int buildInitialFockMatrix();
    int buildInitialDensityMatrix(int&);    
    int computeInitialSCFEnergy(Integral&);
    int computeNewFockMatrix(Integral&);
    int computeNewDensityMatrix(int&);
    int computeNewSCFEnergy(Integral&);
    void rmsD();
    bool convergence();
    void nextStep();
    void printIteration(Integral&);
    int SCF(Integral&, Geometry&);
    

    RHF(){};
    ~RHF(){};

private:
    int* indexcache;
    double* Sorth;
    double* F;
    double* D;
    double* Hcore;
    double* newF;
    double* newD;
    double Eold;
    double dE;
    double dD;
    int Iter;
};

#endif
