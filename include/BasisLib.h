#ifndef BASISLIB_H
#define BASISLIB_H

#include<string>
#include<vector>
#include<tuple>

using namespace std;


class NormCartBF
{
public:
    int power[3];
    int Lmax() const;
    vector <pair<double,double>> alpha_c;
};

class SingleBF{
public:
	int L;
	vector< pair<double,double> > alpha_c;
};

class Element{
public:
	string nameElement;
	vector<SingleBF> bf;
    vector<NormCartBF> normCartBFs;
    int makeNormCart();
};

class BasisLib{
public:
	int findDATA();
	BasisLib(){};
	~BasisLib(){};
	void print() const;
	int loadFromGamessFormat(char* );
	vector< Element > elements;
private:
	vector<string> content;
	int getNewElement(long unsigned int& npos, Element& tmpElement);
};

#endif

