#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <string>
#include <vector>
#include <cmath>

using std::string;
using std::vector;

struct Atom{
    string name;
    int charge;
    double x,y,z;
    //double q;
}; 

class Geometry{
public:
    int charge;
    int Ne;

    vector<Atom> atoms;

    int loadFromXYZFormat(char* );
    int getCharge(string&) const;
    int getNumberOfElectrons();
    double R(long unsigned int&, long unsigned int&) const;

    Geometry(){};
    ~Geometry(){};
};

#endif
