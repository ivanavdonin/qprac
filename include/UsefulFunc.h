#ifndef USEFULFUNC_H
#define USEFULFUNC_H

#include<string>
#include<vector>

using namespace std;

class UsefulFunc{
public:
	UsefulFunc(){};
	~UsefulFunc(){};
	static int loadContent(char*, vector<string>& );
	static int parseString(const string& , vector<string>&);
};

#endif
