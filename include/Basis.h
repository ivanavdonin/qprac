#ifndef BASIS_H
#define BASIS_H

#include "BasisLib.h"
#include "Geometry.h"

class SingleCartGTO : public NormCartBF{
public:
    double x0, y0, z0;
    double Norm(const int &) const;
};

class Basis{
public:
    int create(const BasisLib&, const Geometry&);
    vector<SingleCartGTO> bf;
    void print(const Geometry&) const;
    double Rab(const int&, const int&) const;

    Basis(){};
    ~Basis(){};
};
#endif
