#ifndef __OURMATH_H__
#define __OURMATH_H__

class OurMath
{
public:
	static double d_fact(int);
    static double fact(int);
	static double Inorm(const int &, const double &);
    static double N(const int &, const double &);
    
    static double c(const int&,const int&);
    static double f(const int&,const int&,const int&,const double&,
                    const double&);

    static double A(const int&, const int&, const int&,
                    const int&, const int&, 
                    const double&, const double&, const double&, 
                    const double&);
    
    static double H(const int&, const int&, const int&, const double&, 
                    const double&, const double&);

    static double sumAF(const int&, const int&, 
                        const int&, const int&,
                        const int&, const int&, 
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&);

    static double sumBFfast(const int&, const int&,
                        const int&, const int&, 
                        const int&, const int&, 
                        const int&, const int&,
                        const int&, const int&,
                        const int&, const int&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&);

    static double B(    const int&, const int&,
                        const int&, const int&, const int&,
                        const int&, const int&,
                        const double&, const double&, const double&,
                        const int&, const int&,
                        const double&, const double&, const double&,
                        const double&);

    static double sumBF(const int&, const int&,
                        const int&, const int&, 
                        const int&, const int&, 
                        const int&, const int&,
                        const int&, const int&,
                        const int&, const int&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&, const double&,
                        const double&, const double&);

    static double F(const int&, const double&);

    static int transpose(const int&, double*);
    static int diagMatrix(const int&, double*, double*);
    static int product(const int&, const double*, const double*, double*);
    static int powDiagMatrix(const int&, double*, const double&);
};

#endif
