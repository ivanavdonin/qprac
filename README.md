# Quantum Chemistry Practical Project

The project for practical calculation in quantum chemistry.

## Features

* The Restricted (closed-shell) Hartree-Fock method (HF)
* The second order Møller–Plesset perturbation theory (MP2)

## Requirements 

* Installed LAPACK library
* cmake

## Installation

On Linux
```
sudo apt-get install lapack lapack-devel
git clone https://ivanavdonin@bitbucket.org/ivanavdonin/qprac.git
cd qprac/build
cmake .. && make
make install 
```

## Launching
```
./qprac ../basis/sto-3g ../examples/h2o.in
```
##Input data
* Basis input file Gamess-US type
* Structure input file with coordinates of .xyz format