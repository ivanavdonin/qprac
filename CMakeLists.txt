cmake_minimum_required(VERSION 2.8)
project(qprac)

set(LAPACK_USE_STATIC_LIBS OFF)
set(LAPACK_USE_MULTITHREADED ON)

set(CMAKE_CXX_CPPCHECK "cppcheck")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3 -Wall")

set(SRC_LIST 
	src/print.cpp
	src/timer.cpp
	src/BasisLib.cpp
	src/UsefulFunc.cpp
	src/Basis.cpp
	src/Geometry.cpp
	src/OurMath.cpp
	src/Integral.cpp
	src/RHF.cpp
	src/MP.cpp
	src/main.cpp)

include_directories("include")

find_package(LAPACK COMPONENTS chrono filesystem REQUIRED)

add_executable(${PROJECT_NAME} ${SRC_LIST})

target_link_libraries(${PROJECT_NAME} ${LAPACK_LIBRARIES})

install(TARGETS qprac DESTINATION bin)
