#include "timer.h"
#include "print.h"
#include "RHF.h"
#include "Integral.h"

#include "cmath"
#include "stdio.h"
#include "iostream"

using std::cout;
using std::endl;

#define INDEX(i,j) (i>j) ? (indexcache[i]+j) : (indexcache[j]+i)

int RHF::buildHcore(const Integral& integral){
	indexcache = new int[100000];
    indexcache[0] = 0;
    for(int i=1; i < 100000; i++)
        indexcache[i] = indexcache[i-1] + i;

    size = integral.BasisSize;
    Hcore = new double[size*size];
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++){
            if(j <= i)
                Hcore[i*size+j] =   integral.T[i*(i+1)/2+j] + 
                                    integral.Ven[i*(i+1)/2+j];
            else
                Hcore[i*size+j] =   integral.T[i+j*(j+1)/2] + 
                                    integral.Ven[i+j*(j+1)/2];
        }
    printHcore();
    return 0;
}

int RHF::printHcore(){
    printf("\nThe Core Hamiltonian Matrix: \n\n");
    printf("%2s","");
    for(int i = 0; i < size; i++)
        printf("%12d",i+1);
    printf("\n\n");
    for(int i = 0; i < size; i++){
        printf("%5d",i+1);
        for(int j = 0; j < size; j++)
            printf("%12.5lf",Hcore[i*size+j]);
        printf("\n");
    }
    cout << endl;
    return 0;
}

int RHF::printMOenergies(){
	printf("\n The MO energies \n\n");
	for(int i = size-1; i >= 0; i--){
		printf("%5d%12.5lf\n", i+1, MOenergies[i]);
	}
	printf("\n");
	return 0;
}

int RHF::printMatrix(double* M){
    printf("%2s","");
    for(int i = 0; i < size; i++)
        printf("%12d",i+1);
    printf("\n\n");
    for(int i = 0; i < size; i++){
        printf("%5d",i+1);
        for(int j = 0; j < size; j++)
            printf("%12.7lf",M[i*size+j]);
        printf("\n");
    }
    cout << endl;
    return 0;
}

double* RHF::getMatrix(double* M){
    double* res = new double[size*size];
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++){
            res[i*size+j] =   M[i*(i+1)/2+j];
        }
    return res;
}

double* RHF::getVee(Integral& integral){
    double* res = new double[size*size*size*size];
    for(int i = 0; i < size*size*size*size; i++)
        res[i] = integral.Vee[i];
    return res;
}

int RHF::buildOrthogonalizationMatrix(Integral& integral){
    double* Ls = getMatrix(integral.S);
    double* lambda = new double[size*size];
    double* Sprod = new double[size*size];
    Sorth = new double[size*size];

    OurMath::diagMatrix(size,Ls,lambda);
    printMatrix(lambda);
    OurMath::powDiagMatrix(size,lambda,-0.5);
    printMatrix(lambda);
    OurMath::product(size,Ls,lambda,Sprod);
    OurMath::transpose(size,Ls);
    OurMath::product(size,Sprod,Ls,Sorth);
    OurMath::transpose(size,Ls);

    //delete[] Sprod,lambda, Ls;  warning: right operand of comma operator has no effect

    printf("\n The S^-1/2 matrix \n\n");
    printMatrix(Sorth);

    return 0;
}

int RHF::buildInitialFockMatrix(){
    double* left = new double[size*size];
    F = new double[size*size];

    OurMath::transpose(size,Sorth);
    OurMath::product(size,Sorth,Hcore,left);
    OurMath::transpose(size,Sorth);
    OurMath::product(size,left,Sorth,F);

    printf(" The initial Fock Matrix in the orthogonal AO basis \n\n");
    printMatrix(F);
    return 0;
}

int RHF::buildInitialDensityMatrix(int& Ne){    
    double* initialEnergies = new double[size*size];
    double* eigenvectors = new double[size*size];
    D = new double[size*size];
    double* C = new double[size*size];
    for(int i = 0; i < size*size; i++)
        C[i] = F[i];

    OurMath::diagMatrix(size,C,initialEnergies);
    //Transform the eigenvectors into the original (non-orthogonal) AO basis
    OurMath::product(size,Sorth,C,eigenvectors);
    //Build the density matrix using the occupied MOs
    printf(" The initial MO coefficients matrix \n\n");
    printMatrix(eigenvectors);

    MOenergies = new double[size];
    for (int i = 0; i < size; ++i){
    	MOenergies[i] = initialEnergies[i*(size+1)];
    }

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++){
            D[i*size+j] = 0;
            for(int m = 0; m < Ne/2; m++){
                D[i*size+j] += eigenvectors[i*size+m]*eigenvectors[j*size+m];
            }
        }
        
    printf(" The initial density matrix \n\n");
    printMatrix(D);
    
    return 0;
}

int RHF::computeInitialSCFEnergy(Integral& integral){
    Eold = 0;
    printMatrix(F);
    printMatrix(Hcore);
    printMatrix(D);
     for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        	Eold += D[j*size+i]*(Hcore[i*size+j]+F[i*size+j]);
        	
    printf(" The initial Hartree-Fock total energy: %.15lf\n\n",Eold);


    return 0;
}

int RHF::computeNewFockMatrix(Integral&integral){
    double* Vee = getVee(integral);
    newF = new double[size*size];
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++){
            newF[i*size+j] = Hcore[i*size+j];
            int ij = INDEX(i,j);
            for(int k = 0; k < size; k++)
                for(int l = 0; l < size; l++){
                	int kl = INDEX(k,l);
                    int ik = INDEX(i,k);
                    int jl = INDEX(j,l);
                    int ijkl = INDEX(ij,kl);
                    int ikjl = INDEX(ik,jl);
                    newF[i*size+j] += D[k*size+l]*(2*Vee[ijkl]-Vee[ikjl]);
                }
        }
    return 0;
}

int RHF::computeNewDensityMatrix(int& Ne){
    double* left = new double[size*size];
    
    double* newFbuffer = new double[size*size];
    for (int i = 0; i < size*size; ++i){
        newFbuffer[i] = newF[i];
    }

    OurMath::transpose(size,Sorth);
    OurMath::product(size,Sorth,newFbuffer,left);
    OurMath::transpose(size,Sorth);
    OurMath::product(size,left,Sorth,newFbuffer);

    double* initialEnergies = new double[size*size];
    double* eigenvectors = new double[size*size];
    newD = new double[size*size];
    double* C = new double[size*size];
    for(int i = 0; i < size*size; i++)
        C[i] = newFbuffer[i];

    OurMath::diagMatrix(size,C,initialEnergies);

    MOenergies = new double[size];
    for (int i = 0; i < size; ++i){
    	MOenergies[i] = initialEnergies[i*(size+1)];
    }

    //Transform the eigenvectors into the original (non-orthogonal) AO basis
    OurMath::product(size,Sorth,C,eigenvectors);

    MOvectors = new double[size*size];
    for(int i = 0; i < size*size; i++){
    	MOvectors[i] = eigenvectors[i];
    }

    //printMatrix(MOvectors);
  
    //Build the density matrix using the occupied MOs
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++){
            newD[i*size+j] = 0;
            for(int m = 0; m < Ne/2; m++){
                newD[i*size+j] += eigenvectors[i*size+m]*eigenvectors[j*size+m];
            }
        }
        
    return 0;
}

int RHF::computeNewSCFEnergy(Integral& integral){
    E = 0;
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
            E += newD[i*size+j]*(Hcore[i*size+j]+newF[i*size+j]);
    return 0;
}

void RHF::rmsD(){
    dD = 0;
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
            dD += (newD[i*size+j]-D[i*size+j])*(newD[i*size+j]-D[i*size+j]);
    dD = sqrt(dD);
}

bool RHF::convergence(){
    if((abs(dE) >= deltaE) || (abs(dD) >= deltaD))
        return false;
    else
        return true;     
}

void RHF::nextStep(){
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++){
            F[i*size+j] = newF[i*size+j];
            D[i*size+j] = newD[i*size+j];
        }
    Eold = E;
    Iter++;
}

void RHF::printIteration(Integral& integral){
    printf("%6d%20.12lf%19.12lf%19.12lf\n",Iter,E+integral.Vnn,dE,dD);
}

int RHF::SCF(Integral& integral, Geometry& geom){
    Timer SCFtime;
    maxIter = 200;
    Iter = 0;

    buildHcore(integral);
    buildOrthogonalizationMatrix(integral);
    buildInitialFockMatrix();
    buildInitialDensityMatrix(geom.Ne);
    computeInitialSCFEnergy(integral);

    for(int i = 0; i < 70; i++) printf("―");
    printf("\n");
    printf("%7s%15s%19s%25s\n","Iter","Energy","Change","Density Matrix RMS");
    for(int i = 0; i < 70; i++) printf("―");
    printf("\n");                       
    printf("%6d%20.12lf\n",Iter,Eold+integral.Vnn);
    Iter++;

    while(Iter <= maxIter){
        computeNewFockMatrix(integral);
        if(Iter == 1){
            //printf("\n Fock matrix:\n\n");
            //printMatrix(newF);
        }
        computeNewDensityMatrix(geom.Ne);
        computeNewSCFEnergy(integral);
        dE = Eold - E;
        rmsD();
        printIteration(integral);
        if(convergence()){
        	    for(int i = 0; i < 70; i++) printf("―");
        printf("\n");	
            //printMatrix(MOvectors);
            printf("\nThe energy has been converged!\n\n");
            printf("The final Hartree-Fock total energy: %.12lf\n\n",E+integral.Vnn);
            printTime(SCFtime.stop());
            printMOenergies();
            printf("MO orbitals: \n");
            printMatrix(MOvectors);
            return 0;
        }
        nextStep();
    }
        for(int i = 0; i < 70; i++) printf("―");
    printf("\n");
    printf("\nThe maximum number of interations has been achieved.\n\n");
    printTime(SCFtime.stop());
    return 1;
}
