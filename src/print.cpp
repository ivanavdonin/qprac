#include "print.h"
#include "stdio.h"

void printTime(float sec){
  	printf("time: %02d:%02d:%2.3f (hh:mm:ss) %.2f days | %.2f hours | %.2f minutes | %.3f seconds\n\n", (int) (sec*1.0/3600), 
  															     (int) ((sec-3600*((int)(sec*1.0/3600)))*1.0/60),
  			                 sec-60*((int) ((sec-3600*((int)(sec*1.0/3600)))*1.0/60))-3600*((int)(sec*1.0/3600)),
  			                 																	 sec*1.0/3600/24,
  			                																		sec*1.0/3600,
  			                																		  sec*1.0/60,
  			                																				sec);
}