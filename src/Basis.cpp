#include "Basis.h"
#include "OurMath.h"

#include <iostream>
#include <cmath>

using std::cout;
using std::endl;

int Basis::create(const BasisLib& basislib, const Geometry& geometry){
    bf.clear();
    SingleCartGTO tbf;
    
    for(long unsigned int i = 0; i < geometry.atoms.size(); i++){
        bool found = false;
        for(long unsigned int j = 0; j < basislib.elements.size(); j++){
            string elementName = basislib.elements[j].nameElement.c_str();
            if(geometry.atoms[i].charge == geometry.getCharge(elementName)){
                found = true;
                tbf.x0 = geometry.atoms[i].x;                
                tbf.y0 = geometry.atoms[i].y;                
                tbf.z0 = geometry.atoms[i].z;
                for(long unsigned int k = 0; k < basislib.elements[j].normCartBFs.size(); k++){
                    tbf.alpha_c = basislib.elements[j].normCartBFs[k].alpha_c;
                    for(int q = 0; q < 3; q++){
                        tbf.power[q] = basislib.elements[j].normCartBFs[k].power[q];
                    }
                    bf.push_back(tbf);
                }
            }
        }
        if (!(found)) 
            cout << "warring: there is no basis functions for the element " 
                << geometry.atoms[i].name << endl;    
    }
    return 0;
}

double Basis::Rab(const int& a, const int& b) const {
    return  sqrt((bf[a].x0-bf[b].x0)*(bf[a].x0-bf[b].x0) + 
            (bf[a].y0-bf[b].y0)*(bf[a].y0-bf[b].y0) + 
            (bf[a].z0-bf[b].z0)*(bf[a].z0-bf[b].z0)); 
}

//Normalization factor for a premitive function
//Norm(<the number of premitive>)
double SingleCartGTO::Norm(const int& a) const{
    return OurMath::N(power[0],alpha_c[a].first)*   
           OurMath::N(power[1],alpha_c[a].first)*   
           OurMath::N(power[2],alpha_c[a].first);   
}

void Basis::print(const Geometry& geom) const {
    int primitives = 0;
    for(long unsigned int i = 0; i < bf.size(); i++){
        primitives += bf[i].alpha_c.size();
    }

    cout.width(18);
    cout.fill(' ');
    cout << endl << right << "Summary" << endl;
    for(int i = 0; i < 50; i++) printf("―");
    cout << endl;
    int colomnsize = 30;
    cout.width(colomnsize);
    cout.fill(' ');
    cout << left << " Number of atoms "        	  << geom.atoms.size() << endl;
    cout.width(colomnsize);
    cout.fill(' ');
    cout << left << " Charge "        	  		  << geom.charge       << endl;
    cout.width(colomnsize);
    cout.fill(' ');
    cout << left << " Number of basis functions " << bf.size()         << endl; 
    cout.width(colomnsize);
    cout.fill(' ');
    cout << left << " Number of primitives "      << primitives        << endl;
    cout << endl;
}
