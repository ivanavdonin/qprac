#include "OurMath.h"
#include <cmath>
#include <stdio.h>
#include <iostream>

extern "C" {
 void dsyev_(char *jobz, char *uplo, int *n, double *a, int *lda,
                  double *w, double *work, int *lwork, int *info);
}

extern "C" void boys_func_(int & n, double & t, double & res);

double OurMath::fact(int k){
    double res = 1;
    while(k>1){
        res *= k;
        k--;
    }
    return res;
}

double OurMath::d_fact(int k)
{
	double res = 1;
	while(k>1)
	{
		res *= k;
		k -= 2;
	}
	return res;
}

double OurMath::Inorm(const int &n, const double &gamma)
{
	double res = 0;
	res = d_fact(2*n-1)*sqrt(M_PI)/pow(2,n)/pow(gamma,n+0.5);
	return res;
}

//the normalization factor of one cartesian component 
//of gaussian-type basis function where the sum of k for 
//all components equals maximum angular moment of function
double OurMath::N(const int &k, const double &alpha){
    double kover2 = 0.5*k;
    return pow(2*alpha/M_PI,0.25)*pow(4*alpha,kover2)*pow(d_fact(2*k-1),-0.5);
}

double OurMath::c(const int& n, const int &k){
    if(n == 0)
        return 1;
    else if((k==0)||(n == k))
        return 1;
    else 
        return c(n-1,k-1)+c(n-1,k);
}

//The coefficients in the polynome (x+a)^l*(x+b)^m
double OurMath::f(const int& i, const int &l, const int &m, const double &a, 
                  const double &b){
    double res = 0;
    for(int k = 0; k <= l; k++)
        for(int q = 0; q <= m; q++)
            if(l+m-k-q == i)
                res+=c(l,k)*c(m,q)*pow(a,k)*pow(b,q);
    return res;
}

double OurMath::A(const int & i, const int& r, const int& u,
                  const int& p1,    const int& p2,  
                  const double& PA, const double& PB, const double& CP,
                  const double& p){
    return  pow(-1,i+u)*f(i,p1,p2,PA,PB)*
            fact(i)*pow(CP, i - 2*r - 2*u)*
            pow(0.25/p,r+u)/
            (fact(r)*fact(u)*
            fact(i - 2*r - 2*u));
}

//The Boys function is represented here 
double OurMath::F(const int& v, const double& x){
    if(x <= 1e-20)
        return 1.0/(2*v + 1);
    else if (v > 0)
        return ((2*v-1)*F(v-1,x)-exp(-x))/(2*x);
    else 
        return sqrt(M_PI/x)/2*erf(sqrt(x));
}

double OurMath::sumAF(const int& n1, const int& n2, 
                      const int& l1, const int& l2,
                      const int& m1, const int& m2, 
                      const double& PAx, const double& PAy, const double& PAz,
                      const double& PBx, const double& PBy, const double& PBz,
                      const double& CPx, const double& CPy, const double& CPz,
                      const double& p){
    double res = 0;
    double CPsquared = CPx*CPx + CPy*CPy + CPz*CPz;
    for(int x = 0; x <= n1 + n2; x++)
        for(int r1 = 0; r1 <= x/2; r1++)
            for(int u1 = 0; u1 <= (x-2*r1)/2; u1++)
    for(int y = 0; y <= l1 + l2; y++)
        for(int r2 = 0; r2 <= y/2; r2++)
            for(int u2 = 0; u2 <= (y-2*r2)/2; u2++)
    for(int z = 0; z <= m1 + m2; z++)
        for(int r3 = 0; r3 <= z/2; r3++)
            for(int u3 = 0; u3 <= (z-2*r3)/2; u3++){
                int v = x + y + z - 2*(r1 + r2 + r3) - (u1 + u2 + u3); 
                res +=  A(x,r1,u1,n1,n2,PAx,PBx,CPx,p)*
                        A(y,r2,u2,l1,l2,PAy,PBy,CPy,p)*
                        A(z,r3,u3,m1,m2,PAz,PBz,CPz,p)*F(v,p*CPsquared);
            }
    return res;
}

double OurMath::H(const int& L, const int& p1, const int& p2, const double& a, 
                    const double&b, const double& p){
    if ((p1 == 0)&&(p2 == 0)){
                    return 1.0;
    }

    if (p1 == 1){
        if (p2 == 0)
            switch ( L ) {
                case 0:
                    return a;
                case 1:
                    return 0.25/p;
            }
        if (p2 == 1)
            switch ( L ) {
                case 0:
                    return a*b+0.5/p;
                case 1:
                    return (a + b)*0.25/p;
                case 2:
                    return (0.25/p)*(0.25/p);
            }
    }
    if (p1 == 2){
        if (p2 == 0)
            switch ( L ) {
                case 0:
                    return a*a+0.5/p;
                case 1:
                    return a*0.25/p;
                case 2:
                    return (0.25/p)*(0.25/p);
            }
        if (p2 == 1)
            switch ( L ) {
                case 0:
                    return a*a*b + (2*a + b)*0.25/p;
                case 1:
                    return (2*a*b + a*a)*0.25/p + 6*(0.25/p)*(0.25/p);
                case 2:
                    return (2*a + b)*(0.25/p)*(0.25/p);
                case 3:
                    return (0.25/p)*(0.25/p)*(0.25/p);
            }
        if (p2 == 2)
            switch ( L ) {
                case 0:
                    return a*a*b*b+(a*a+4*a*b+b*b)*0.25/p+12*(0.25/p)*(0.25/p);
                case 1:
                    return a*b*(a+b)*0.25/p+12*(a+b)*(0.25/p)*(0.25/p);
                case 2:
                    return (a*a+4*a*b+b*b)*(0.25/p)*(0.25/p)+
                                        12*(0.25/p)*(0.25/p)*(0.25/p);
                case 3:
                    return 2*(a+b)*(0.25/p)*(0.25/p)*(0.25/p);
                case 4:
                    return (0.25/p)*(0.25/p)*(0.25/p)*(0.25/p);
            }
    }           
    return -1;
}

double OurMath::sumBFfast(  const int& n1, const int& n2,
                        const int& n3, const int& n4, 
                        const int& l1, const int& l2, 
                        const int& l3, const int& l4,
                        const int& m1, const int& m2,
                        const int& m3, const int& m4,
                        const double& PAx, const double& PAy, const double& PAz,
                        const double& PBx, const double& PBy, const double& PBz,
                        const double& QCx, const double& QCy, const double& QCz,
                        const double& QDx, const double& QDy, const double& QDz,
                        const double& QPx, const double& QPy, const double& QPz,
                        const double& p1, const double& p2){
    double res = 0;
    double d = 0.25/p1 + 0.25/p2;
    double QPsquared = QPx*QPx + QPy*QPy + QPz*QPz;

    double CI[n1+n2+n3+n4+1]; for(int i = 0; i <= n1+n2+n3+n4; i++) CI[i] = 0;
    double CJ[l1+l2+l3+l4+1]; for(int i = 0; i <= l1+l2+l3+l4; i++) CJ[i] = 0;
    double CK[m1+m2+m3+m4+1]; for(int i = 0; i <= m1+m2+m3+m4; i++) CK[i] = 0;

    for(int L = 0; L <= n1+n2; L++)
        for(int M = 0; M <= n3+n4; M++)
            for(int u = 0; u <= (L+M)/2; u++){
                CI[L+M-u] += H(L,n1,n2,PAx,PBx,p1)*pow(-1,M+u)*
                             H(M,n3,n4,QCx,QDx,p2)*
                             fact(L+M)*pow(QPx,(L+M)-2*u)/
                             (fact(u)*fact(L+M-2*u)*pow(d,L+M-u));
            }
    
    for(int L = 0; L <= l1+l2; L++)
        for(int M = 0; M <= l3+l4; M++)
            for(int u = 0; u <= (L+M)/2; u++){
                CJ[L+M-u] += H(L,l1,l2,PAy,PBy,p1)*pow(-1,M+u)*
                             H(M,l3,l4,QCy,QDy,p2)*
                             fact(L+M)*pow(QPy,(L+M)-2*u)/
                             (fact(u)*fact(L+M-2*u)*pow(d,L+M-u));
            }

    for(int L = 0; L <= m1+m2; L++)
        for(int M = 0; M <= m3+m4; M++)
            for(int u = 0; u <= (L+M)/2; u++){
                CK[L+M-u] += H(L,m1,m2,PAz,PBz,p1)*pow(-1,M+u)*
                             H(M,m3,m4,QCz,QDz,p2)*
                             fact(L+M)*pow(QPz,(L+M)-2*u)/
                             (fact(u)*fact(L+M-2*u)*pow(d,L+M-u));
            }                  
    
    for(int I = 0; I <= n1+n2+n3+n4; I++)
        for(int J = 0; J <= l1+l2+l3+l4; J++)
            for(int K = 0; K <= m1+m2+m3+m4; K++){
                res += CI[I]*CJ[J]*CK[K]*F(I+J+K,QPsquared*0.25/d);
            }

    return res;
}

double OurMath::B(  const int& i1, const int& i2,
                    const int& r1, const int& r2, const int& u,
                    const int& n1, const int& n2,
                    const double& PA, const double& PB, const double& p1,
                    const int& n3, const int& n4,
                    const double& QC, const double& QD, const double& p2,
                    const double& QP){

    double d = 0.25/p1 + 0.25/p2;
    return pow(-1,i2)*f(i1,n1,n2,PA,PB)*f(i2,n3,n4,QC,QD)*
            fact(i1)*fact(i2)*1.0/(pow(4*p1,i1)*pow(4*p2,i2)*pow(d,i1+i2))*
            pow(4*p1,r1)*pow(4*p2,r2)*pow(d,r1+r2)*1.0/
                (fact(r1)*fact(r2)*fact(i1-2*r1)*fact(i2-2*r2))*
            fact(i1+i2-2*(r1+r2))*pow(-1,u)*pow(QP,i1+i2-2*(r1+r2)-2*u)*pow(d,u)*1.0/
                (fact(u)*fact(i1+i2-2*(r1+r2)-2*u));
}

double OurMath::sumBF(  const int& n1, const int& n2,
                        const int& n3, const int& n4, 
                        const int& l1, const int& l2, 
                        const int& l3, const int& l4,
                        const int& m1, const int& m2,
                        const int& m3, const int& m4,
                        const double& PAx, const double& PAy, const double& PAz,
                        const double& PBx, const double& PBy, const double& PBz,
                        const double& QCx, const double& QCy, const double& QCz,
                        const double& QDx, const double& QDy, const double& QDz,
                        const double& QPx, const double& QPy, const double& QPz,
                        const double& p1, const double& p2){
    double res = 0;
    double d = 0.25/p1 + 0.25/p2;
    double QPsquared = QPx*QPx + QPy*QPy + QPz*QPz;

    for(int i1 = 0; i1 <= n1 + n2; i1++)
        for(int i2 = 0; i2 <= n3 + n4; i2++)
            for(int r1 = 0; r1 <= i1/2; r1++)
                for(int r2 = 0; r2 <= i2/2; r2++)
                    for(int u = 0; u <= (i1+i2)/2-r1-r2; u++)     
    for(int j1 = 0; j1 <= l1 + l2; j1++)
        for(int j2 = 0; j2 <= l3 + l4; j2++)
            for(int s1 = 0; s1 <= j1/2; s1++)
                for(int s2 = 0; s2 <= j2/2; s2++)
                    for(int v = 0; v <= (j1+j2)/2-s1-s2; v++)     
    for(int k1 = 0; k1 <= m1 + m2; k1++)
        for(int k2 = 0; k2 <= m3 + m4; k2++)
            for(int t1 = 0; t1 <= k1/2; t1++)
                for(int t2 = 0; t2 <= k2/2; t2++)
                    for(int w = 0; w <= (k1+k2)/2-t1-t2; w++){
                        int I = i1 + i2 - 2*(r1 + r2) - u;
                        int J = j1 + j2 - 2*(s1 + s2) - v;
                        int K = k1 + k2 - 2*(t1 + t2) - w;     
                        int q = I + J + K;
                        //int Li = i1 + i2;
                        //int Mr = -2*(r1 + r2);
                        //int Lj = j1 + j2;
                        //int Ms = -2*(s1 + s2);
                        //int Lk = k1 + k2;
                        //int Mt = -2*(t1 + t2);
                        
                        /*if(QPsquared <= 1e20){
                            QPx = 0;
                            QPy = 0;
                            QPz = 0;
                        }*/
                        printf("Bx = %5.3f By = %5.3f Bz = %5.3f F = %5.3f q = %d QP^2 = %5.3f QPx^zz = %5.3f i1 = %d f1 = %.3f i2 = %d f2 = %.3f\n",
                        B(i1,i2,r1,r2,u,n1,n2,PAx,PBx,p1,n3,n4,QCx,QDx,p2,QPx),
                        B(j1,j2,s1,s2,v,l1,l2,PAy,PBy,p1,l3,l4,QCy,QDy,p2,QPy),
                        B(k1,k2,t1,t2,w,m1,m2,PAz,PBz,p1,m3,m4,QCz,QDz,p2,QPz),
                        F(q,QPsquared*1.0/(4*d)),i1+i2-2*(r1+r2)-2*u,QPsquared,pow(QPx,i1+i2-2*(r1+r2)-2*u),
                        i1, f(i1,n1,n2,PAx,PBx), i2, f(i2,n3,n4,QCx,QDx));

                        res += 
                        B(i1,i2,r1,r2,u,n1,n2,PAx,PBx,p1,n3,n4,QCx,QDx,p2,QPx)*
                        B(j1,j2,s1,s2,v,l1,l2,PAy,PBy,p1,l3,l4,QCy,QDy,p2,QPy)*
                        B(k1,k2,t1,t2,w,m1,m2,PAz,PBz,p1,m3,m4,QCz,QDz,p2,QPz)*
                        F(q,QPsquared*1.0/(4*d));
                        
                        //if (QPsquared <= 1e-20)
                        //    printf("QP is zero\n");

                        if (res<-10e7){
                            printf("F = %.6f\n q = %d\n QP^2 = %.18f QPx^qq = %.15f\n",F(q,QPsquared/(4*d)),i1+i2-2*(r1+r2)-2*u,QPsquared,pow(QPx,i1+i2-2*(r1+r2)-2*u));
                        }
                    }

    //printf("QP^2 = %.15f\n",res); 
    return res;
}

int OurMath::transpose(const int& size, double* m){
    double* res;
    res = new double[size*size];
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
            res[i*size+j] = m[i+j*size];

    for(int i = 0; i < size*size; i++)
        m[i] = res[i];

    //delete[] res;
    return 0;
}


 //LAPACK matrix diagonalization
//m is rewritten as a matrix of eigenvectors (columns),
//v is a diagonal matrix of corresponding eigenvalues.
int OurMath::diagMatrix(const int& size, double* m, double* v) {
    int n=size;
    char jobz='V';
    char uplo='U';
    int lda=n;
    int lwork=n*6;
    double* work = new double [lwork];
    int info;
    dsyev_(&jobz, &uplo, &n, m, &lda, v, work, &lwork, &info);
    if (info!=0) std::cout << "Error of diagonalization:  "<<info<<'\n';
    delete [] work;

    //diagonal look
    for(int i = 1; i < size; i++){
        v[i*(size+1)] = v[i];
        v[i] = 0;
    }

    transpose(size,m);

    return info;
}

int OurMath::product(const int& size, const double* m1, const double* m2, 
                                                                  double* res){
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++){
            res[i*size+j] = 0;
            for(int q = 0; q < size; q++)
                res[i*size+j] += m1[i*size+q]*m2[q*size+j];
        }
    return 0;
}

int OurMath::powDiagMatrix(const int& size, double* m, 
                                        const double& power){
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++){
            if(i == j)
                	m[i*(size+1)] = pow(m[i*(size+1)],power);
            else 
                m[i*size+j] = 0;
        }

    return 0;
}
