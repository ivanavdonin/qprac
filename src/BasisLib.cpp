#include"BasisLib.h"
#include"UsefulFunc.h"
#include"OurMath.h"

#include<iostream>
#include<sstream>
#include<fstream>
#include<cstdlib>
#include<cmath>


#include <iomanip>

int Element::makeNormCart(){
	normCartBFs.clear();
	for(long unsigned int i = 0; i < bf.size(); i++){
        	NormCartBF tmpNormCartBF;
         	for (int j = 0; j<=bf[i].L; j++)
         		for (int k = 0; k<=bf[i].L-j; k++){
				tmpNormCartBF.power[0]=bf[i].L-j-k;
				tmpNormCartBF.power[1]=k;
				tmpNormCartBF.power[2]=j;
				tmpNormCartBF.alpha_c = bf[i].alpha_c;
				for (long unsigned int l=0; l< bf[i].alpha_c.size(); l++){
					double alpha = tmpNormCartBF.alpha_c[l].first;
					tmpNormCartBF.alpha_c[l].second /= sqrt(OurMath::Inorm(j,2.0*alpha));
					tmpNormCartBF.alpha_c[l].second /= sqrt(OurMath::Inorm(k,2.0*alpha));
					tmpNormCartBF.alpha_c[l].second /= sqrt(OurMath::Inorm(bf[i].L-j-k,2.0*alpha)); 
				}
				double normAll = 0;
				for (long unsigned int p = 0; p < tmpNormCartBF.alpha_c.size(); p++)
					for (long unsigned int q = 0; q < tmpNormCartBF.alpha_c.size(); q++){
						double gamma = tmpNormCartBF.alpha_c[p].first +
						               tmpNormCartBF.alpha_c[q].first; 
						normAll +=
							tmpNormCartBF.alpha_c[p].second*
							tmpNormCartBF.alpha_c[q].second*
							OurMath::Inorm(j,gamma)* 
							OurMath::Inorm(k,gamma)* 
							OurMath::Inorm(bf[i].L-j-k,gamma);
					}
				std::cout << "Lmax = " << tmpNormCartBF.Lmax() <<  " Factor = "<<normAll<<'\n';
				for (long unsigned int l=0; l< bf[i].alpha_c.size(); l++)
					tmpNormCartBF.alpha_c[l].second /= sqrt(normAll);
				normCartBFs.push_back(tmpNormCartBF);
			}	
	}
    return 0;
}

int BasisLib::findDATA(){
	vector<string> tmpCont;
	for (int i = 0 ; i < (int) content.size(); i++){
		UsefulFunc::parseString(content[i], tmpCont);
		if (tmpCont.size()==1)
			if (tmpCont[0]=="$DATA")
				return i;
	}
	return -1;
}

int BasisLib::loadFromGamessFormat(char* filename){
	UsefulFunc::loadContent(filename, content);
	long unsigned int nPos = findDATA();
	nPos++;
	Element tmpElement;
	elements.clear();
    cout << "Normalization of basis functions" << endl;
	while (getNewElement(nPos,tmpElement) == 0 ){
        tmpElement.makeNormCart();
		elements.push_back(tmpElement);		
    }
    return 0;
}

int BasisLib::getNewElement(long unsigned int& npos, Element& tmpElement){
    if ((npos < 0)||(npos > content.size()))
        return -1;

    vector<string> tmp;
    UsefulFunc::parseString(content[npos],tmp);

    if (tmp[0] == "$END") 
        return 1;
          
    tmpElement.nameElement = tmp[0];
    npos++;

    tmpElement.bf.clear();
    
    UsefulFunc::parseString(content[npos],tmp);
    
    while((tmp.size() != 0) && (tmp[0] != "$END")){
        SingleBF tmpbf;
        SingleBF tmpbf2;
        if (tmp[0] == "S")
            tmpbf.L = 0;
        else if (tmp[0] == "P")
            tmpbf.L = 1;
        else if (tmp[0] == "L"){
            tmpbf.L = 0;
            tmpbf2.L = 1;
        }
        else if (tmp[0] == "D")
            tmpbf.L = 2;
        else if (tmp[0] == "F")
            tmpbf.L = 3;
        else if (tmp[0] == "G")
            tmpbf.L = 4;
        else if (tmp[0] == "H")
            tmpbf.L = 5;

        int q = atoi(tmp[1].c_str());
        vector<string> tmpcoef;
        for (long unsigned int j = npos+1; j <= npos+q; j++){
            UsefulFunc::parseString(content[j],tmpcoef);
            if (tmp[0] != "L"){
                pair<double,double> a_c = make_pair(atof(tmpcoef[1].c_str()),atof(tmpcoef[2].c_str()));
                tmpbf.alpha_c.push_back(a_c);
            }
            else {
                pair<double,double> a_c1 = make_pair(atof(tmpcoef[1].c_str()),atof(tmpcoef[2].c_str()));
                pair<double,double> a_c2 = make_pair(atof(tmpcoef[1].c_str()),atof(tmpcoef[3].c_str()));
                tmpbf.alpha_c.push_back(a_c1);
                tmpbf2.alpha_c.push_back(a_c2);
            }
        }
        tmpElement.bf.push_back(tmpbf);
        if(tmp[0] == "L")
            tmpElement.bf.push_back(tmpbf2);
        
        npos += q+1;
        
        UsefulFunc::parseString(content[npos],tmp);

    }
    if (content[npos] != "$END"){
        npos++;
    }
    
    return 0;  
}

void BasisLib::print() const
{
    int nbf = 0;
    int ngp = 0;
    printf("\n\n");
    cout << endl << "The list of elements in the basis file" << endl;
    int ewidth = 15;
    int bwidth = 20;
    int pwidth = 20;

    printf("\n\n");
    cout.width(ewidth); 
    cout << left << "Element";
    cout.width(bwidth);    
    cout << "Basis Functions";
    cout.width(pwidth);
    cout << "Primitives" << endl;
    for(int i = 0; i < 50; i++) printf("―"); 
    cout << endl;
    for (long unsigned int i = 0; i < elements.size(); i++){
        int ngpf = 0;
        nbf += elements[i].bf.size();
        for (long unsigned int j = 0; j < elements[i].bf.size(); j++)
            //cout << elements[i].bf[j].L << endl;//" " << elements[i].bf[j].c_Alpha.size() << endl;
            ngpf += elements[i].bf[j].alpha_c.size();
        ngp += ngpf;
        cout.width(ewidth); 
        cout << left << elements[i].nameElement;
        cout.width(bwidth);     
        cout << elements[i].bf.size(); 
        cout.width(pwidth);
        cout << left << ngpf << endl;
    }

    cout << endl;
    int colomnsize = 30;
	
	cout.width(18);
    cout.fill(' ');
    cout << endl << right << "Overall" << endl;
    for(int i = 0; i < 50; i++) printf("―"); 
    cout << endl;
    cout.width(colomnsize);
    cout.fill(' ');
    cout << left << " Number of elements: "          << elements.size() << endl;
    cout.width(colomnsize);
    cout.fill(' ');
    cout << left << " Number of function: " << nbf          << endl; 
    cout.width(colomnsize);
    cout.fill(' ');
    cout << left << " Number of primitives: "        << ngp             << endl;
    cout << endl;
}


int NormCartBF::Lmax() const {
    return power[0]+power[1]+power[2];
}
