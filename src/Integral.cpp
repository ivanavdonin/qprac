#include "timer.h"
#include "print.h"
#include "Integral.h"
#include "Geometry.h"
#include "OurMath.h"

#include "iostream"
#include "stdio.h"
#include <iomanip>
#include "cmath"


using std::cout;
using std::endl;

#define INDEX(i,j) (i>j) ? (indexcache[i]+j) : (indexcache[j]+i)


int Integral::InitAndCalcAll(const Basis& basis, const Geometry& geom){

	indexcache = new long unsigned int[100000];
    indexcache[0] = 0;
    for(int i=1; i < 100000; i++)
        indexcache[i] = indexcache[i-1] + i;

    BasisSize = basis.bf.size();
    
    Vnn = calcVnn(geom);
    printf("\nNuclear repultion energy = %.15lf\n\n",Vnn);

    S = new double[BasisSize*(BasisSize+1)/2];
    printf("\nOverlap integrals:\n\n");
    int iterS = 0;
    for(long unsigned int i = 0; i < basis.bf.size(); i++)
        for(long unsigned int j = 0; j <= i; j++){
            double result = calcS(i,j,basis);
            S[iterS] = result;
            iterS++;
            printf("%5lu%6lu%23.15lf \n",i+1,j+1,result);     
        }

    T = new double[BasisSize*(BasisSize+1)/2];
    printf("\nKinetic-energy integrals:\n\n");
    int iterT = 0;
    for(long unsigned int i = 0; i < basis.bf.size(); i++)
        for(long unsigned int j = 0; j <= i; j++){
            double result = calcT(i,j,basis);
            T[iterT] = result;
            iterT++;
            printf("%5lu%6lu%23.15lf \n",i+1,j+1,result);     
        }
    
    Ven = new double[BasisSize*(BasisSize+1)/2];
    printf("\nNuclear-attraction integrals:\n\n");
    int iterVen = 0;
    for(long unsigned int i = 0; i < basis.bf.size(); i++)
        for(long unsigned int j = 0; j <= i; j++){
            double result = 0;
            for(long unsigned int N = 0; N < geom.atoms.size(); N++){
                result -= geom.atoms[N].charge*calcVen(i,N,j,basis,geom);
            }
            Ven[iterVen] = result;
            iterVen++;
            printf("%5lu%6lu%23.15lf \n",i+1,j+1,result);
        }
    
    Vee = new double[BasisSize*BasisSize*BasisSize*BasisSize];
    for(int i = 0; i < BasisSize*BasisSize*BasisSize*BasisSize; i++){
    	Vee[i] = 0;
    }

    printf("\nElectron-electron repulsion integrals:\n\n");
    Timer eriTime;
    int iterVee = 0;
    double result = 0;
    for(long unsigned int i = 0; i < basis.bf.size(); i++)
        for(long unsigned int k = 0; k < basis.bf.size(); k++){
            for(long unsigned int j = 0; j <= i; j++)
                for(long unsigned int l = 0; l <= k; l++){
                    if((i*(i+1)/2+j >= k*(k+1)/2+l)){
                        result = calcMDVee(i,j,k,l,basis);

                        if (abs(result) > 1e-16){
                        	int ij 	= INDEX(i,j);
                        	int kl 	= INDEX(k,l);
			                int ijkl 	= INDEX(ij,kl);
                        	Vee[ijkl] = result;
                        	iterVee++;
                        	printf("%7lu%6lu%6lu%6lu%23.15lf\n",i+1,j+1,k+1,l+1,result);
                        }
                    }
                }
        }

    numberVee = iterVee;

    printf("\nNumber of non-zero permutationally unique two-electron repulsion integrals: %d\n\n", numberVee);
    printTime(eriTime.stop());
      
    return 0;
}

double Integral::calcVnn(const Geometry& geom){
    double res = 0;
    for(long unsigned int i = 0; i < geom.atoms.size(); i++)
        for(long unsigned int j = i+1; j < geom.atoms.size(); j++)
            res += geom.atoms[i].charge*geom.atoms[j].charge/geom.R(i,j);
    return res;
}

//separation px2 -- power of x in the 2nd GTO
////k-th and l-th GTO in BF
double Integral::overlapGTO(const int& k, const int& l,
                            const int& px2, const int& py2, const int& pz2, 
                            const int& i, const int& j,                        
                            const Basis& basis){
    if((px2 < 0) || (py2 < 0) || (pz2 < 0))
        return 0;

    double a = basis.bf[i].alpha_c[k].first;
    double b = basis.bf[j].alpha_c[l].first;
    double mu = a*b/(a+b);
    double p = a + b;
    double integral = M_PI/p;
    
    double angx = 0;
    double PAx = (basis.bf[i].x0-basis.bf[j].x0)*b/p; 
    double PBx = (basis.bf[i].x0-basis.bf[j].x0)*a/p;
    for(int x = 0; x <= (basis.bf[i].power[0]+px2)/2; x++){
         angx += OurMath::f(2*x,basis.bf[i].power[0],px2,PAx,PBx)*
         OurMath::d_fact(2*x-1)/pow(2*p,x);
    }
    double angy = 0;
    double PAy = (basis.bf[i].y0-basis.bf[j].y0)*b/p;
    double PBy = (basis.bf[i].y0-basis.bf[j].y0)*a/p;
    for(int y = 0; y <= (basis.bf[i].power[1]+py2)/2; y++){
         angy += OurMath::f(2*y,basis.bf[i].power[1],py2,PAy,PBy)*
         OurMath::d_fact(2*y-1)/pow(2*p,y);
    }
    double angz = 0;
    double PAz = (basis.bf[i].z0-basis.bf[j].z0)*b/p;
    double PBz = (basis.bf[i].z0-basis.bf[j].z0)*a/p;
    for(int z = 0; z <= (basis.bf[i].power[2]+pz2)/2; z++){
         angz += OurMath::f(2*z,basis.bf[i].power[2],pz2,PAz,PBz)*
         OurMath::d_fact(2*z-1)/pow(2*p,z);
    }
    return  basis.bf[i].alpha_c[k].second*
            basis.bf[j].alpha_c[l].second*
            pow(integral,1.5)*
            exp(-mu*basis.Rab(i,j)*basis.Rab(i,j))*
            angx*angy*angz;
}

//overlap integral (Taketa)
double Integral::calcS(const int& i, const int& j, const Basis& basis){
    double res = 0;
    for(long unsigned int k = 0; k < basis.bf[i].alpha_c.size(); k++)
        for(long unsigned int l = 0; l < basis.bf[j].alpha_c.size(); l++){
            res += overlapGTO(k,l,
                basis.bf[j].power[0],basis.bf[j].power[1],basis.bf[j].power[2],
                i, j, basis);
        }
    return res;
}

//kinetic-energy integral
double Integral::calcT(const int& i, const int& j, const Basis& basis){
    double res = 0;

    for(long unsigned int k = 0; k < basis.bf[i].alpha_c.size(); k++)
        for(long unsigned int l = 0; l < basis.bf[j].alpha_c.size(); l++){
            //double a1 = basis.bf[i].alpha_c[k].first;
            double a2 = basis.bf[j].alpha_c[l].first;
            int n2 = basis.bf[j].power[0];
            int l2 = basis.bf[j].power[1];
            int m2 = basis.bf[j].power[2];
        
            res += a2*(2*(n2+l2+m2)+3)*overlapGTO(k,l,n2,l2,m2,i,j,basis)-
                2*a2*a2*(overlapGTO(k,l,n2+2,l2,m2,i,j,basis)+
                         overlapGTO(k,l,n2,l2+2,m2,i,j,basis)+
                         overlapGTO(k,l,n2,l2,m2+2,i,j,basis))-
                0.5*(n2*(n2-1)*overlapGTO(k,l,n2-2,l2,m2,i,j,basis)+
                     l2*(l2-1)*overlapGTO(k,l,n2,l2-2,m2,i,j,basis)+
                     m2*(m2-1)*overlapGTO(k,l,n2,l2,m2-2,i,j,basis)); 
        }
            //int n2 = basis.bf[j].power[0];
            //int l2 = basis.bf[j].power[1];
            //int m2 = basis.bf[j].power[2];
    return res;
}

//nuclear-electron attraction integral <i|1/r|j> (Taketa)
double Integral::calcVen(const int& i, const int& c, const int& j, 
                                                     const Basis& basis,
                                                     const Geometry& geom){
    double res = 0;
    for(long unsigned int k = 0; k < basis.bf[i].alpha_c.size(); k++)
        for(long unsigned int l = 0; l < basis.bf[j].alpha_c.size(); l++){
            double n1 = basis.bf[i].power[0];
            double l1 = basis.bf[i].power[1];
            double m1 = basis.bf[i].power[2];
            double n2 = basis.bf[j].power[0];
            double l2 = basis.bf[j].power[1];
            double m2 = basis.bf[j].power[2];
            double a = basis.bf[i].alpha_c[k].first;
            double b = basis.bf[j].alpha_c[l].first;
            double mu = a*b/(a+b);
            double p = a + b;
            double integral = M_PI/p;

            double PAx = (basis.bf[i].x0-basis.bf[j].x0)*b/p;
            double PBx = (basis.bf[i].x0-basis.bf[j].x0)*a/p;
            double CPx = ((basis.bf[i].x0-geom.atoms[c].x)*a+
                          (basis.bf[j].x0-geom.atoms[c].x)*b)/p;

            double PAy  =   (basis.bf[i].y0-basis.bf[j].y0)*b/p;
            double PBy  =   (basis.bf[i].y0-basis.bf[j].y0)*a/p;
            double CPy  =  ((basis.bf[i].y0-geom.atoms[c].y)*a+
                            (basis.bf[j].y0-geom.atoms[c].y)*b)/p;

            double PAz  =   (basis.bf[i].z0-basis.bf[j].z0)*b/p;
            double PBz  =   (basis.bf[i].z0-basis.bf[j].z0)*a/p;
            double CPz  =  ((basis.bf[i].z0-geom.atoms[c].z)*a+
                            (basis.bf[j].z0-geom.atoms[c].z)*b)/p;
            //double CPsquared = CPx*CPx + CPy*CPy + CPz*CPz;

            res +=  basis.bf[i].alpha_c[k].second*basis.bf[j].alpha_c[l].second*
                    2*integral*
                    exp(-mu*basis.Rab(i,j)*basis.Rab(i,j))*
                    OurMath::sumAF(n1,n2,l1,l2,m1,m2,
                                    PAx,PAy,PAz,
                                    PBx,PBy,PBz,
                                    CPx,CPy,CPz,p);
        }
    return res;
}

//electron-electron repulsion integral 
double Integral::calcVee(const int& i, const int& j,
                         const int& k, const int& l, const Basis& basis){
    double res = 0;
    for(long unsigned int ki = 0; ki < basis.bf[i].alpha_c.size(); ki++)
        for(long unsigned int kj = 0; kj < basis.bf[j].alpha_c.size(); kj++)
    for(long unsigned int kk = 0; kk < basis.bf[k].alpha_c.size(); kk++)
        for(long unsigned int kl = 0; kl < basis.bf[l].alpha_c.size(); kl++){
            double n1 = basis.bf[i].power[0];
            double l1 = basis.bf[i].power[1];
            double m1 = basis.bf[i].power[2];
            double n2 = basis.bf[j].power[0];
            double l2 = basis.bf[j].power[1];
            double m2 = basis.bf[j].power[2];
            double n3 = basis.bf[k].power[0];
            double l3 = basis.bf[k].power[1];
            double m3 = basis.bf[k].power[2];
            double n4 = basis.bf[l].power[0];
            double l4 = basis.bf[l].power[1];
            double m4 = basis.bf[l].power[2];
            double a1 = basis.bf[i].alpha_c[ki].first;
            double b1 = basis.bf[j].alpha_c[kj].first;
            double a2 = basis.bf[k].alpha_c[kk].first;
            double b2 = basis.bf[l].alpha_c[kl].first;
            double mu1 = a1*b1/(a1+b1);
            double mu2 = a2*b2/(a2+b2);
            double p1 = a1 + b1;
            double p2 = a2 + b2;

            double PAx  =   (basis.bf[i].x0-basis.bf[j].x0)*b1/p1;
            double PBx  =   (basis.bf[i].x0-basis.bf[j].x0)*a1/p1;
            double QCx  =   (basis.bf[k].x0-basis.bf[l].x0)*b2/p2;
            double QDx  =   (basis.bf[k].x0-basis.bf[l].x0)*a2/p2;
            double QPx  =   (basis.bf[i].x0*a1+basis.bf[j].x0*b1)/p1-
                            (basis.bf[k].x0*a2+basis.bf[l].x0*b2)/p2;
             
            double PAy  =   (basis.bf[i].y0-basis.bf[j].y0)*b1/p1;
            double PBy  =   (basis.bf[i].y0-basis.bf[j].y0)*a1/p1;
            double QCy  =   (basis.bf[k].y0-basis.bf[l].y0)*b2/p2;
            double QDy  =   (basis.bf[k].y0-basis.bf[l].y0)*a2/p2;
            double QPy  =   (basis.bf[i].y0*a1+basis.bf[j].y0*b1)/p1-
                            (basis.bf[k].y0*a2+basis.bf[l].y0*b2)/p2;

            double PAz  =   (basis.bf[i].z0-basis.bf[j].z0)*b1/p1;
            double PBz  =   (basis.bf[i].z0-basis.bf[j].z0)*a1/p1;
            double QCz  =   (basis.bf[k].z0-basis.bf[l].z0)*b2/p2;
            double QDz  =   (basis.bf[k].z0-basis.bf[l].z0)*a2/p2;
            double QPz  =   (basis.bf[i].z0*a1+basis.bf[j].z0*b1)/p1-
                            (basis.bf[k].z0*a2+basis.bf[l].z0*b2)/p2;


            res += basis.bf[i].alpha_c[ki].second*
                   basis.bf[j].alpha_c[kj].second*
                   basis.bf[k].alpha_c[kk].second*
                   basis.bf[l].alpha_c[kl].second*
                   2*M_PI*M_PI/p1/p2*sqrt(M_PI/(p1+p2))*
                   exp(-mu1*basis.Rab(i,j)-mu2*basis.Rab(k,l))*
                   OurMath::sumBFfast(n1,n2,n3,n4,l1,l2,l3,l4,m1,m2,m3,m4,
                                  PAx,PAy,PAz,PBx,PBy,PBz,
                                  QCx,QCy,QCz,QDx,QDy,QDz,
                                  QPx,QPy,QPz,p1,p2);
        }
    return res;
}

//MD section
int Integral::calcRntuv(double* R,
	const int & tmax, const int & umax, const int & vmax,
	const double & p,
	const double & xPA, const double & yPA, const double & zPA){
//	define constant 
	const int nmax=tmax+umax+vmax;
	const int ndim=(tmax+1)*(umax+1)*(vmax+1);
	const int tdim=(umax+1)*(vmax+1);
	const int udim=(vmax+1);
//	make Fn array
	double* Fn = new double [nmax+1];
	double x2=p*(xPA*xPA+yPA*yPA+zPA*zPA);
	if (x2<1.e-4) {
		for (int n=0; n<(nmax+1); n++) Fn[n]=1.0/(2.0*n+1.0);
	} else {
		Fn[0]=erf(sqrt(x2))*sqrt(acos(0.0)*0.5/x2);
		for (int n=1; n<(nmax+1); n++) Fn[n]=((2*n-1)*Fn[n-1]-exp(-x2))*0.5/x2;
	};
//	calc Rnt00
	for (int n=0; n<=nmax; n++) R[n*ndim]=pow(-2.0*p, n)*Fn[n];
	delete [] Fn;
	int xR;
	if (tmax>0){
	for (int n=0; n<nmax; n++) R[n*ndim+tdim]=xPA*R[(n+1)*ndim];	// calc Rn,1,0,0;
		for (int t=1; t<tmax; t++)					// calc Rn,ti,00;
			for (int n=0; n<nmax-t; n++){
				xR=(n+1)*ndim+t*tdim;				// coordinate Rn+1,t,0,0
				R[xR-ndim+tdim]=xPA*R[xR]+t*R[xR-tdim];
			};
	};
//	calc Rntu0
	if (umax>0){
		for (int t=0; t<=tmax; t++)					// calc Rn,ti,1,0;
			for (int n=0; n<=(nmax-t); n++)
				R[n*ndim+t*tdim+udim]=yPA*R[(n+1)*ndim+t*tdim];
		for (int u=1; u<umax; u++)					// calc Rn,ti,ui,0;
			for (int t=0; t<=tmax; t++)
				for (int n=0; n<nmax-t-u; n++){
					xR=(n+1)*ndim+t*tdim+udim*u;		// coordinate Rn+1,t,u,0
					R[xR-ndim+udim]=yPA*R[xR]+u*R[xR-udim];
			};
	};
//	calc Rntuv
	if (vmax>0){
		for (int t=0; t<=tmax; t++)					// calc Rn,ti,ui,1;
			for (int u=0; u<=umax; u++)
				for (int n=0; n<=(nmax-t-u); n++)
					R[n*ndim+t*tdim+u*udim+1]=zPA*R[(n+1)*ndim+t*tdim+u*udim];
		for (int v=1; v<vmax; v++)					// calc Rn,ti,ui,vi;
			for (int t=0; t<=tmax; t++)
				for (int u=0; u<=umax; u++)
					for (int n=0; n<nmax-t-u-v; n++){
						xR=(n+1)*ndim+t*tdim+udim*u+v;	// coordinate Rn+1,t,u,v
						R[xR-ndim+1]=zPA*R[xR]+v*R[xR-1];
			};
	};
	return 0;
}

int Integral::calcEijt(double* E, const int & imax, const int & jmax,
	const double& p, const double& mu, const double& xPA, const double & xPB){
	int tmax=imax+jmax+1;
	double* Etmp = new double [tmax*tmax];
	for (int i=0; i<tmax*tmax;i++) Etmp[i]=0;
	Etmp[0]=exp(-mu*(xPA-xPB)*(xPA-xPB));
	for (int i = 0; i< imax; i++)
		for (int t=0; t<=i; t++){
			Etmp[(i+1)*tmax+t]   += Etmp[i*tmax+t]*xPA;
			Etmp[(i+1)*tmax+t+1] += Etmp[i*tmax+t]*0.5/p;
			Etmp[(i+1)*tmax+t-1] += Etmp[i*tmax+t]*t; // эта строка не будет учтена когда t=0;
		};	
	for (int j=0; j<jmax; j++)
		for (int t=0; t<=(j+imax); t++){
			Etmp[(imax+j+1)*tmax+t]   += Etmp[(imax+j)*tmax+t]*xPB;
			Etmp[(imax+j+1)*tmax+t+1] += Etmp[(imax+j)*tmax+t]*0.5/p;
			Etmp[(imax+j+1)*tmax+t-1] += Etmp[(imax+j)*tmax+t]*t; // the same situation
		};
	for (int t=0; t<tmax; t++) E[t]=Etmp[tmax*tmax-tmax+t];
	delete [] Etmp;
	return 0;
}

double Integral::Vijkl(const int& nx1, const int& ny1, const int& nz1, const double& x1, const double& y1, const double& z1, const double& alpha1,
                                  const int& nx2, const int& ny2, const int& nz2, const double& x2, const double& y2, const double& z2, const double& alpha2,
                                  const int& nx3, const int& ny3, const int& nz3, const double& x3, const double& y3, const double& z3, const double& alpha3,
                                  const int& nx4, const int& ny4, const int& nz4, const double& x4, const double& y4, const double& z4, const double& alpha4){
	const double p=alpha1+alpha2;
	const double mup=alpha1*alpha2/p;
	const double xp=(alpha1*x1+alpha2*x2)/p;
	const double yp=(alpha1*y1+alpha2*y2)/p;
	const double zp=(alpha1*z1+alpha2*z2)/p;
	const double q=alpha3+alpha4;
	const double muq=alpha3*alpha4/q;
	const double xq=(alpha3*x3+alpha4*x4)/q;
	const double yq=(alpha3*y3+alpha4*y4)/q;
	const double zq=(alpha3*z3+alpha4*z4)/q;
	const double al=p*q/(p+q);
	const int t1max=nx1+nx2+1;
	const int u1max=ny1+ny2+1;
	const int v1max=nz1+nz2+1;
	double* E1ij = new double [t1max];
	double* E1kl = new double [u1max];
	double* E1mn = new double [v1max];
	calcEijt(E1ij, nx1, nx2, p, mup, xp-x1, xp-x2);
	calcEijt(E1kl, ny1, ny2, p, mup, yp-y1, yp-y2);
	calcEijt(E1mn, nz1, nz2, p, mup, zp-z1, zp-z2);

	const int t2max=nx3+nx4+1;
	const int u2max=ny3+ny4+1;
	const int v2max=nz3+nz4+1;
	double* E2ij = new double [t2max];
	double* E2kl = new double [u2max];
	double* E2mn = new double [v2max];
	calcEijt(E2ij, nx3, nx4, q, muq, xq-x3, xq-x4);
	calcEijt(E2kl, ny3, ny4, q, muq, yq-y3, yq-y4);
	calcEijt(E2mn, nz3, nz4, q, muq, zq-z3, zq-z4);
	
	const int tmax=t1max+t2max-1;
	const int umax=u1max+u2max-1;
	const int vmax=v1max+v2max-1;

	double* Rntuv = new double [tmax*umax*vmax*(tmax+umax+vmax-2)];

	calcRntuv(Rntuv, tmax-1, umax-1, vmax-1 , al, xp-xq, yp-yq, zp-zq);

	double vt=0;
	for (int t1=0; t1<t1max; t1++)
	  for (int u1=0; u1<u1max; u1++)
	    for (int v1=0; v1<v1max; v1++)
	      for (int t2=0; t2<t2max; t2++)
	        for (int u2=0; u2<u2max; u2++)
	          for (int v2=0; v2<v2max; v2++)
		      	vt+=E1ij[t1]*E1kl[u1]*E1mn[v1]*E2ij[t2]*E2kl[u2]*E2mn[v2]*Rntuv[(t1+t2)*umax*vmax+(u2+u1)*vmax+(v1+v2)]*pow(-1,v2+u2+t2);
			
	delete [] E1ij;
	delete [] E1kl;
	delete [] E1mn;
	delete [] E2ij;
	delete [] E2kl;
	delete [] E2mn;
	delete [] Rntuv;
	return vt*8*acos(0.0)*acos(0.0)/p/q*sqrt(acos(0.0)*2/(p+q));
}

double Integral::calcMDVee(const int& i, const int& j,
                         const int& k, const int& l, const Basis& basis){
    double res = 0;
    for(long unsigned int ki = 0; ki < basis.bf[i].alpha_c.size(); ki++)
        for(long unsigned int kj = 0; kj < basis.bf[j].alpha_c.size(); kj++)
    for(long unsigned int kk = 0; kk < basis.bf[k].alpha_c.size(); kk++)
        for(long unsigned int kl = 0; kl < basis.bf[l].alpha_c.size(); kl++){
        	double n1 = basis.bf[i].power[0];
            double l1 = basis.bf[i].power[1];
            double m1 = basis.bf[i].power[2];
            double n2 = basis.bf[j].power[0];
            double l2 = basis.bf[j].power[1];
            double m2 = basis.bf[j].power[2];
            double n3 = basis.bf[k].power[0];
            double l3 = basis.bf[k].power[1];
            double m3 = basis.bf[k].power[2];
            double n4 = basis.bf[l].power[0];
            double l4 = basis.bf[l].power[1];
            double m4 = basis.bf[l].power[2];

            res += basis.bf[i].alpha_c[ki].second*
                   basis.bf[j].alpha_c[kj].second*
                   basis.bf[k].alpha_c[kk].second*
                   basis.bf[l].alpha_c[kl].second*
                   Vijkl(n1, l1, m1, basis.bf[i].x0, basis.bf[i].y0, basis.bf[i].z0, basis.bf[i].alpha_c[ki].first,
                         n2, l2, m2, basis.bf[j].x0, basis.bf[j].y0, basis.bf[j].z0, basis.bf[j].alpha_c[kj].first,
                         n3, l3, m3, basis.bf[k].x0, basis.bf[k].y0, basis.bf[k].z0, basis.bf[k].alpha_c[kk].first,
                         n4, l4, m4, basis.bf[l].x0, basis.bf[l].y0, basis.bf[l].z0, basis.bf[l].alpha_c[kl].first);
        }
    return res;
}

//end MD section

int Integral::printMatrix(double* M){
    printf("\nThe intergral matrix  \n\n");
    printf("%2s","");
    for(int i = 0; i < BasisSize; i++)
        printf("%12d",i+1);
    printf("\n\n");
    for(int i = 0; i < BasisSize; i++){
        printf("%5d",i+1);
        for(int j = 0; j < BasisSize; j++)
            if(i>=j)
                printf("%12.5lf",M[i*(i+1)/2+j]);
            else
                printf("%12.5lf",M[i+j*(j+1)/2]);
        printf("\n");
    }
    return 0;
}
