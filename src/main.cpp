#include "timer.h"
#include "print.h"
#include "BasisLib.h"
#include "iostream"
#include "Geometry.h"
#include "Basis.h"
#include "Integral.h"
#include "RHF.h"
#include "MP.h"

int main(int argc,  char* argv[]){
	if (argc < 3) return 1;

	Timer totalTime;

	BasisLib basisLib;
	basisLib.loadFromGamessFormat(argv[1]);
    basisLib.print();
    
    Geometry geom;
    geom.loadFromXYZFormat(argv[2]);

    Basis basis;
    basis.create(basisLib,geom);
    basis.print(geom);
    
    Integral integral;
    integral.InitAndCalcAll(basis,geom);

    RHF rhf;
    rhf.deltaE = 1e-12;
    rhf.deltaD = 1e-12;
    rhf.SCF(integral,geom);

    MP mp2;
    mp2.mp2(geom, integral, rhf);
    printf("Total MP2 energy = %.12lf\n", rhf.E + integral.Vnn + mp2.Emp2);

    printf("\n\nThe program has finished fine!\n\n");
	printTime(totalTime.stop());
	return 0;
}