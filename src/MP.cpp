#include "timer.h"
#include "print.h"
#include <MP.h>
#include "RHF.h"
#include "Integral.h"
#include "Geometry.h"

#include "cmath"
#include "stdio.h"
#include "iostream"

#define INDEX(i,j) (i>j) ? (indexcache[i]+j) : (indexcache[j]+i)

int MP::getIntegrals(Integral& integral){
	size = integral.BasisSize;
	VeeAO = new double[size*size*size*size];
	for(int i = 0; i < size*size*size*size; i++)
		VeeAO[i] = integral.Vee[i];
	return 0;
}

int MP::getOrbitals(RHF& rhf){ 
	orbitals = new double[size*size];
	energies = new double[size];
	for(int i = 0; i < size*size; i++){
		orbitals[i] = rhf.MOvectors[i];
	}
	for(int i = 0; i < size; i++){
		energies[i] = rhf.MOenergies[i];
	}
	return 0;
}

int MP::transformIntegrals(RHF& rhf){
	printf("Transforming two-electron integrals from the AO basis to the MO basis\n");
	VeeMO = new double[size*size*size*size];
	for(int i = 0; i < size; i++)
		for(int j = 0; j < size; j++)
			for(int k = 0; k < size; k++)
				for(int l = 0; l < size; l++){
					int ij = INDEX(i,j);
					int kl = INDEX(k,l);
					int ijkl = INDEX(ij,kl);
					VeeMO[ijkl] = 0;
					for(int a = 0; a < size; a++)
						for(int b = 0; b < size; b++)
							for(int c = 0; c < size; c++)
								for(int d = 0; d < size; d++){
									int ab = INDEX(a,b);
									int cd = INDEX(c,d);
									int abcd = INDEX(ab,cd);
									VeeMO[ijkl] += orbitals[a*size+i]
												  *orbitals[b*size+j]
												  *VeeAO[abcd]
												  *orbitals[c*size+k]
												  *orbitals[d*size+l];
									}
				}
	return 0;
}
int MP::computeMP2Energy(){
	printf("Computing MP2 energy\n");
	Emp2 = 0;
	for(int i = 0; i < Ne/2; ++i)
		for(int j = 0; j < Ne/2; ++j){
			for (int a = Ne/2; a < size ; ++a){
				for(int b = Ne/2; b < size; ++b){
					int ia = INDEX(i,a);
					int jb = INDEX(j,b);
					int iajb = INDEX(ia,jb);
					int ib = INDEX(i,b);
					int ja = INDEX(j,a);
					int ibja = INDEX(ib,ja);
					Emp2 += VeeMO[iajb]*(2*VeeMO[iajb]-VeeMO[ibja])/(energies[i]+energies[j]-energies[a]-energies[b]);
				}
			}

		}
	
	printf("MP2 energy = %.12lf\n", Emp2);
	return 0;
}

int MP::mp2(Geometry& geometry, Integral& integral, RHF& rhf){
	printf("\nMP2 calculations\n\n");

	Ne = geometry.Ne;

	indexcache = new int[100000];
    indexcache[0] = 0;
    for(int i=1; i < 100000; i++)
    indexcache[i] = indexcache[i-1] + i;

	getIntegrals(integral);
	getOrbitals(rhf);

	Timer transformTime;
	transformIntegrals(rhf);
	printTime(transformTime.stop());

	computeMP2Energy();
return 0;
}