#include "Geometry.h" 
#include <fstream>
#include <iomanip>
#include "iostream"

int Geometry::getCharge(string& name) const {
    if(name == "HYDROGEN"	|| name == "H"	|| name == "1"	) return 1;                                             
    if(name == "HELIUM"		|| name == "He" || name == "2"  ) return 2;                                             
    if(name == "LITHIUM"	|| name == "Li" || name == "3"  ) return 3;                                             
   	if(name == "BERYLLIUM"	|| name == "Be" || name == "4"  ) return 4;                                             
   	if(name == "BORON"		|| name == "B" 	|| name == "5" 	) return 5;                                             
   	if(name == "CARBON"		|| name == "C" 	|| name == "6" 	) return 6;                                             
   	if(name == "NITROGEN"	|| name == "N" 	|| name == "7" 	) return 7;                                             
    if(name == "OXYGEN"		|| name == "O" 	|| name == "8" 	) return 8;                                             
    if(name == "FLUORIN"	|| name == "F" 	|| name == "9" 	) return 9;                                             
    if(name == "NEON"		|| name == "Ne"	|| name == "10"	) return 10;                                            
	if(name == "SODIUM"		|| name == "Na"	|| name == "11"	) return 11;                                            
	if(name == "MAGNESIUM"	|| name == "Mg"	|| name == "12"	) return 12;                                            
	if(name == "ALUMINIUM"	|| name == "Al"	|| name == "13"	) return 13;                                            
	if(name == "SILICON"	|| name == "Si"	|| name == "14"	) return 14;                                            
	if(name == "PHOSPHORUS"	|| name == "P"	|| name == "15"	) return 15;                                            
	if(name == "SULFUR"		|| name == "S" 	|| name == "16" ) return 16;                                            
	if(name == "CHLORINE"	|| name == "Cl" || name == "17" ) return 17;                                            
	if(name == "ARGON"		|| name == "Ar" || name == "18" ) return 18;
    else return 0;
}

int Geometry::loadFromXYZFormat(char* filename){
    atoms.clear();
    std::ifstream inp(filename);
    string tmpStr;
    long unsigned int number = 0;
    inp >> number;
    if (number == 0)
        return 1;
    getline(inp,tmpStr);
    charge = 0;
    inp >> charge;
    getline(inp,tmpStr);
    Atom tAtom;
    tAtom.charge = 0;
    while(inp >> std::setprecision(16) >> tAtom.name >> tAtom.x >> tAtom.y >> tAtom.z){
        tAtom.charge = getCharge(tAtom.name);
        atoms.push_back(tAtom);
    }
    inp.close();

    getNumberOfElectrons();

    if(atoms.size() != number){
        atoms.clear();
        return 1;
    }
    return 0;
}

int Geometry::getNumberOfElectrons(){
    int res = 0;
    for(long unsigned int i = 0; i < atoms.size(); i++)
        res += atoms[i].charge;
    Ne = res - charge;
    return 0;
}

double Geometry::R(long unsigned int& a, long unsigned int& b) const {
    return sqrt((atoms[a].x - atoms[b].x)*(atoms[a].x - atoms[b].x) +
                (atoms[a].y - atoms[b].y)*(atoms[a].y - atoms[b].y) +
                (atoms[a].z - atoms[b].z)*(atoms[a].z - atoms[b].z));
}

