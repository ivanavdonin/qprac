#include"UsefulFunc.h"
#include<sstream>
#include<fstream>

int UsefulFunc::loadContent(char* filename, vector<string>& cont ) {
	ifstream inp(filename);
	string tmpStr;
	cont.clear();
	while ( getline(inp, tmpStr) )
		cont.push_back(tmpStr);
	inp.close();
    return 0;
}

int UsefulFunc::parseString(const string& inputStr, vector<string>& result) {
	result.clear();
	stringstream ss;
	ss<<inputStr;
	string tStr;
	while (ss>>tStr) result.push_back(tStr);
    return 0;
}


