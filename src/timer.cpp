#include "timer.h"
#include "time.h"

Timer::Timer(){
	start = clock();
}

float Timer::stop(){
	clock_t stop = clock();
    float diff((float)stop - (float)start);
  	float sec = diff/CLOCKS_PER_SEC;
	return sec;
}